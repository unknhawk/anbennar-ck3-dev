﻿
bring_the_moormen_to_heel = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_misc.dds"
	}

	desc = bring_the_moormen_to_heel_desc
	selection_tooltip = bring_the_moormen_to_heel_tooltip
	decision_group_type = major
	ai_goal = yes

	is_shown = {
		is_ruler = yes
		is_landed = yes
		NOT = {  # Can only do it once.
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:bring_the_moormen_to_heel
			}
		}
		highest_held_title_tier >= 4 # Kings and Emperors only.
		culture = { has_cultural_pillar = heritage_alenic }
		# The ruler picking the decision should not already control Westmoors.
		NOT = {
			completely_controls_region = custom_westmoors
		}
		# The ruler asking it must be a King or Emperor next to the Moormen.
		OR = {
			has_title = title:k_gawed
			has_title = title:k_adshaw
			# has_title = title:k_coddorran # uncomment when it's re-added
		}
		# Cannot be a moorman
		NOT = {
			culture = culture:moorman
		}
		government_has_flag = government_is_feudal
		any_county_in_region = {
			region = custom_westmoors
			culture = culture:moorman
		}
	}
	
	is_valid = {
		trigger_if = {
			limit = {
				root.faith = { has_doctrine = doctrine:doctrine_no_head }
			}
			piety_level >= 4
		}
		trigger_if = {
			limit = {
				exists = root.faith.religious_head
			}
			root.faith.religious_head = {
				opinion = {
					target = root
					value >= very_high_positive_opinion
				}
			}
			piety_level >= 3
		}
		
		root.religion = { is_in_family = rf_pantheonic }

		has_realm_law = crown_authority_3
		
		# Must control core territories of Gawed.
		completely_controls = title:d_gawed
		completely_controls = title:d_westmounts
		completely_controls = title:d_ginnfield
	}

	is_valid_showing_failures_only = {
		is_capable_adult = yes
		is_imprisoned = no
		is_independent_ruler = yes
		is_at_war = no
	}

	cost = {
		gold = 500
		piety = 1000
	}

	effect = {
		show_as_tooltip = {
			house = {
				add_house_modifier = {
					modifier = westmoors_subjugator_modifier
					years = 100
				}
			}
		}
		custom_tooltip = westmoors_subjugator_modifier_effects_tooltip
		trigger_event = {
			id = alenic_frontier_decisions.1001
		}
		hidden_effect = {
			add_to_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:bring_the_moormen_to_heel
			}
		}
	}
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 15
		ai_value_modifier = {
			ai_boldness = 1.0
		}
	}
}

the_moormen_kneel_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_misc.dds"
	}

	desc = the_moormen_kneel_decision_desc
	selection_tooltip = the_moormen_kneel_decision_tooltip
	decision_group_type = major
	ai_goal = yes

	is_shown = {
		is_ruler = yes
		is_landed = yes
		NOT = {  # Can only do it once.
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:the_moormen_kneel_decision
			}
		}
		
		exists = house
		house = { has_house_modifier =  westmoors_subjugator_modifier }

		# Cannot be a moorman
		NOT = {
			culture = culture:moorman
		}
		government_has_flag = government_is_feudal
	}
	
	is_valid = {
		completely_controls_region = custom_westmoors
		title:k_westmoors = { title_held_years >= 15 }
	}

	is_valid_showing_failures_only = {
		is_capable_adult = yes
		is_imprisoned = no
		is_independent_ruler = yes
		is_at_war = no
	}

	effect = {
		show_as_tooltip = {
			the_moormen_kneel_decision_effect = yes
		}
		trigger_event = {
			id = alenic_frontier_decisions.1003
		}
		hidden_effect = {
			add_to_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:the_moormen_kneel_decision
			}
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 15
		ai_value_modifier = {
			ai_boldness = 1.0
		}
	}
}
