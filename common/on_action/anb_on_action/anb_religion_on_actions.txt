﻿on_16th_birthday = {
	on_actions = {
		patron_16th_birthday
	}
}
patron_16th_birthday = {
	trigger = {
		is_ai = yes
		age = 16
		OR = {
			anb_faith_has_patron_gods_trigger = yes
			anb_has_custom_patron_gods_trigger = yes
		}
	}
	effect = {
		anb_give_characters_patrons = yes
	}
}

on_faith_created = {
	on_actions = {
		anb_on_faith_created
	}
}

on_faith_conversion = {
	on_actions = {
		anb_on_faith_conversion_patron_god
	}
}
on_character_faith_change = {
	on_actions = {
		anb_on_faith_conversion_patron_god
	}	
}
# scope:old_faith
anb_on_faith_created = {
	effect = {
		if = {
			limit = {
				faith = {
					OR = {
						has_doctrine = doctrine_patron_deity_castellos
						has_doctrine = doctrine_patron_deity_the_dame
						has_doctrine = doctrine_patron_deity_esmaryal
						has_doctrine = doctrine_patron_deity_falah
						has_doctrine = doctrine_patron_deity_nerat
						has_doctrine = doctrine_patron_deity_adean
						has_doctrine = doctrine_patron_deity_ryala
						has_doctrine = doctrine_patron_deity_balgar
						has_doctrine = doctrine_patron_deity_ara
						has_doctrine = doctrine_patron_deity_munas
						has_doctrine = doctrine_patron_deity_nathalyne
						has_doctrine = doctrine_patron_deity_begga
						has_doctrine = doctrine_patron_deity_minara
						has_doctrine = doctrine_patron_deity_the_dame_munas
						has_doctrine = doctrine_patron_deity_the_dame_adean
						has_doctrine = doctrine_patron_deity_ara_the_dame
						has_doctrine = doctrine_patron_deity_esmaryal_the_dame_ara
						has_doctrine = doctrine_patron_deity_adean_minara
						has_doctrine = doctrine_patron_deity_castellos_esmaryal_falah
						has_doctrine = doctrine_patron_deity_castellos_falah_nerat
						has_doctrine = doctrine_patron_deity_south_castellos
						has_doctrine = doctrine_patron_deity_uelos
						has_doctrine = doctrine_patron_deity_wip						
					}
				}
			}
			faith = {
				switch = {
					trigger = has_doctrine
					doctrine_patron_deity_castellos = {
						add_to_variable_list = { name = patron_gods target = flag:castellos }
					}
					doctrine_patron_deity_the_dame = {
						add_to_variable_list = { name = patron_gods target = flag:the_dame }
					}
					doctrine_patron_deity_esmaryal = {
						add_to_variable_list = { name = patron_gods target = flag:esmaryal }
					}
					doctrine_patron_deity_falah = {
						add_to_variable_list = { name = patron_gods target = flag:falah }
					}
					doctrine_patron_deity_nerat = {
						add_to_variable_list = { name = patron_gods target = flag:nerat }
					}
					doctrine_patron_deity_adean = {
						add_to_variable_list = { name = patron_gods target = flag:adean }
					}
					doctrine_patron_deity_ryala = {
						add_to_variable_list = { name = patron_gods target = flag:ryala }
					}
					doctrine_patron_deity_balgar = {
						add_to_variable_list = { name = patron_gods target = flag:balgar }
					}
					doctrine_patron_deity_ara = {
						add_to_variable_list = { name = patron_gods target = flag:ara }
					}
					doctrine_patron_deity_munas = {
						add_to_variable_list = { name = patron_gods target = flag:munas }
					}
					doctrine_patron_deity_nathalyne = {
						add_to_variable_list = { name = patron_gods target = flag:nathalyne }
					}
					doctrine_patron_deity_begga = {
						add_to_variable_list = { name = patron_gods target = flag:begga }
					}
					doctrine_patron_deity_minara = {
						add_to_variable_list = { name = patron_gods target = flag:minara }
					}
					doctrine_patron_deity_the_dame_munas = {
						add_to_variable_list = { name = patron_gods target = flag:the_dame }
						add_to_variable_list = { name = patron_gods target = flag:munas }
					}
					doctrine_patron_deity_the_dame_adean = {
						add_to_variable_list = { name = patron_gods target = flag:the_dame }
						add_to_variable_list = { name = patron_gods target = flag:adean }
					}
					doctrine_patron_deity_ara_the_dame = {
						add_to_variable_list = { name = patron_gods target = flag:the_dame }
						add_to_variable_list = { name = patron_gods target = flag:ara }
					}
					doctrine_patron_deity_esmaryal_the_dame_ara = {
						add_to_variable_list = { name = patron_gods target = flag:esmaryal }
						add_to_variable_list = { name = patron_gods target = flag:the_dame }
						add_to_variable_list = { name = patron_gods target = flag:ara }
					}
					doctrine_patron_deity_adean_minara = {
						add_to_variable_list = { name = patron_gods target = flag:adean }
						add_to_variable_list = { name = patron_gods target = flag:the_dame }
					}
					doctrine_patron_deity_castellos_esmaryal_falah = {
						add_to_variable_list = { name = patron_gods target = flag:castellos }
						add_to_variable_list = { name = patron_gods target = flag:esmaryal }
						add_to_variable_list = { name = patron_gods target = flag:falah }
					}
					doctrine_patron_deity_castellos_falah_nerat = {
						add_to_variable_list = { name = patron_gods target = flag:castellos }
						add_to_variable_list = { name = patron_gods target = flag:falah }
						add_to_variable_list = { name = patron_gods target = flag:nerat }
					}
					doctrine_patron_deity_south_castellos = {
						add_to_variable_list = { name = patron_gods target = flag:castellos }
					}
					doctrine_patron_deity_uelos = {
						add_to_variable_list = { name = patron_gods target = flag:uelos }
					}
					doctrine_patron_deity_wip = {
						add_to_variable_list = { name = patron_gods target = flag:wip }  
					}
				}
			}
		}
	}
	on_actions = {
		anb_on_faith_conversion_patron_god
	}
}
# scope:old_faith
anb_on_faith_conversion_patron_god = {
	effect = {
		if = {
			limit = {
				has_variable = patron_deity
			}
			remove_variable = patron_deity
		}
		if = {
			limit = {
				has_variable = anb_patron_gods_cd
			}
			remove_variable = anb_patron_gods_cd
		}
		if = {
			limit = {
				anb_has_patron_god_modifier_trigger = yes
			}
			anb_remove_all_patron_modifiers = yes
		}
	}
}