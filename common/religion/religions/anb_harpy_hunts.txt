﻿harpy_hunts_religion = {
	family = rf_harpy
	graphical_faith = dharmic_gfx
	doctrine = harpy_hostility_doctrine

	#Main Group
	doctrine = doctrine_no_head	
	doctrine = doctrine_gender_female_dominated
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_polygamy
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_accepted
	doctrine = doctrine_adultery_men_accepted
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_any_dynasty_member_crime
	doctrine = doctrine_deviancy_accepted
	doctrine = doctrine_witchcraft_accepted

	#Clerical Functions
	doctrine = doctrine_clerical_function_alms_and_pacification
	doctrine = doctrine_clerical_gender_female_only
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	#Funeral tradition
	doctrine = doctrine_funeral_sky_burial
	
	traits = {
		virtues = {
			trusting
			fickle
			brave
			poet
		}
		sins = {
			stubborn
			paranoid
			craven
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {	#TODO
		{ name = "holy_order_jaherian_exemplars" }
		{ name = "holy_order_faith_maharatas" }
	}

	holy_order_maa = { armored_footmen }	#todo

	localization = {
		#HighGod - Firanya
		HighGodName = the_hunt_high_god_name
		HighGodName2 = the_hunt_high_god_name
		HighGodNamePossessive = the_hunt_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_SHE
		HighGodHerselfHimself = CHARACTER_HERHIS_HER
		HighGodHerHis = CHARACTER_HERHIM_HER
		HighGodNameAlternate = the_hunt_high_god_name_alternate
		HighGodNameAlternatePossessive = the_hunt_high_god_name_alternate_possessive

		#Creator
		CreatorName = the_hunt_creator_god_name
		CreatorNamePossessive = the_hunt_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_SHE
		CreatorHerHis = CHARACTER_HERHIS_HER
		CreatorHerHim = CHARACTER_HERHIM_HER

		#HealthGod
		HealthGodName = the_hunt_health_god_name
		HealthGodNamePossessive = the_hunt_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod
		FertilityGodName = the_hunt_fertility_god_name
		FertilityGodNamePossessive = the_hunt_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = the_hunt_wealth_god_name
		WealthGodNamePossessive = the_hunt_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_SHE
		WealthGodHerHis = CHARACTER_HERHIS_HER
		WealthGodHerHim = CHARACTER_HERHIM_HER

		#HouseholdGod
		HouseholdGodName = the_hunt_household_god_name
		HouseholdGodNamePossessive = the_hunt_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod
		FateGodName = the_hunt_fate_god_name
		FateGodNamePossessive = the_hunt_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_SHE
		FateGodHerHis = CHARACTER_HERHIS_HER
		FateGodHerHim = CHARACTER_HERHIM_HER

		#KnowledgeGod
		KnowledgeGodName = the_hunt_knowledge_god_name
		KnowledgeGodNamePossessive = the_hunt_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_SHE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HER
		KnowledgeGodHerHim = CHARACTER_HERHIM_HER

		#WarGod
		WarGodName = the_hunt_war_god_name
		WarGodNamePossessive = the_hunt_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_SHE
		WarGodHerHis = CHARACTER_HERHIS_HER
		WarGodHerHim = CHARACTER_HERHIM_HER

		#TricksterGod
		TricksterGodName = the_hunt_trickster_god_name
		TricksterGodNamePossessive = the_hunt_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_SHE
		TricksterGodHerHis = CHARACTER_HERHIS_HER
		TricksterGodHerHim = CHARACTER_HERHIM_HER

		#NightGod
		NightGodName = the_hunt_night_god_name
		NightGodNamePossessive = the_hunt_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_SHE
		NightGodHerHis = CHARACTER_HERHIS_HER
		NightGodHerHim = CHARACTER_HERHIM_HER

		#WaterGod
		WaterGodName = the_hunt_water_god_name
		WaterGodNamePossessive = the_hunt_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_SHE
		WaterGodHerHis = CHARACTER_HERHIS_HER
		WaterGodHerHim = CHARACTER_HERHIM_HER


		PantheonTerm = the_hunt_high_god_name
		PantheonTerm2 = the_hunt_high_god_name
		PantheonTerm3 = the_hunt_high_god_name
		PantheonTermHasHave = pantheon_term_has
		GoodGodNames = {
			the_hunt_high_god_name
			the_hunt_high_god_name_alternate
		}
		
		DevilName = the_hunt_devil_name
		DevilNamePossessive = the_hunt_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_SHE
		DevilHerHis = CHARACTER_HERHIS_HER
		DevilHerselfHimself = the_hunt_devil_herselfhimself
		EvilGodNames = {
			the_hunt_devil_name
			the_hunt_evil_god_malevolent_dark
		}
		HouseOfWorship = the_hunt_house_of_worship
		HouseOfWorship2 = the_hunt_house_of_worship
		HouseOfWorship3 = the_hunt_house_of_worship
		HouseOfWorshipPlural = the_hunt_house_of_worship_plural
		ReligiousSymbol = the_hunt_religious_symbol
		ReligiousSymbol2 = the_hunt_religious_symbol
		ReligiousSymbol3 = the_hunt_religious_symbol
		ReligiousText = the_hunt_religious_text
		ReligiousText2 = the_hunt_religious_text
		ReligiousText3 = the_hunt_religious_text
		ReligiousHeadName = the_hunt_religious_head_title
		ReligiousHeadTitleName = the_hunt_religious_head_title_name
		DevoteeMale = the_hunt_devotee_male
		DevoteeMalePlural = the_hunt_devotee_male_plural
		DevoteeFemale = the_hunt_devotee_female
		DevoteeFemalePlural = the_hunt_devotee_female_plural
		DevoteeNeuter = the_hunt_devotee_neuter
		DevoteeNeuterPlural = the_hunt_devotee_neuter_plural
		PriestMale = the_hunt_priest
		PriestMalePlural = the_hunt_priest_plural
		PriestFemale = the_hunt_priest
		PriestFemalePlural = the_hunt_priest_plural
		PriestNeuter = the_hunt_priest
		PriestNeuterPlural = the_hunt_priest_plural
		AltPriestTermPlural = the_hunt_priest_term_plural
		BishopMale = the_hunt_bishop
		BishopMalePlural = the_hunt_bishop_plural
		BishopFemale = the_hunt_bishop
		BishopFemalePlural = the_hunt_bishop_plural
		BishopNeuter = the_hunt_bishop
		BishopNeuterPlural = the_hunt_bishop_plural
		DivineRealm = the_hunt_positive_afterlife
		DivineRealm2 = the_hunt_positive_afterlife
		DivineRealm3 = the_hunt_positive_afterlife
		PositiveAfterLife = the_hunt_positive_afterlife
		PositiveAfterLife2 = the_hunt_positive_afterlife
		PositiveAfterLife3 = the_hunt_positive_afterlife
		NegativeAfterLife = the_hunt_negative_afterlife
		NegativeAfterLife2 = the_hunt_negative_afterlife
		NegativeAfterLife3 = the_hunt_negative_afterlife
		DeathDeityName = the_hunt_death_name
		DeathDeityNamePossessive = the_hunt_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_SHE
		DeathDeityHerHis = CHARACTER_HERHIS_HER
		DeathDeityHerHim = CHARACTER_HERHIM_HER

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		hunt_of_the_crane = { 
			color =  { 143 176 234 }
			icon = old_bulwari_sun_cult

			holy_site = heunthume
			holy_site = firanyalen
			holy_site = tazh_qel_araksa
			holy_site = misarallen
			holy_site = ebbusubtu

			doctrine = tenet_astrology
			doctrine = tenet_harpy_hunt
			doctrine = tenet_sun_cult_syncretism
			
		}

		hunt_of_the_eagle = { 
			color =  { 173 228 219 }
			icon = old_bulwari_sun_cult
			
			holy_site = heunthume
			holy_site = firanyalen
			holy_site = siadunaiun
			holy_site = misarallen
			holy_site = ebbusubtu

			doctrine = tenet_pursuit_of_power
			doctrine = tenet_harpy_hunt
			doctrine = tenet_warmonger
			
			doctrine = doctrine_homosexuality_shunned
			doctrine = doctrine_clerical_function_recruitment
		}
		
		hunt_of_the_myna = { 
			color =  { 217 147 125 }
			icon = old_bulwari_sun_cult

			holy_site = heunthume
			holy_site = firanyalen
			holy_site = siadunaiun
			holy_site = misarallen
			holy_site = ebbusubtu

			doctrine = tenet_ritual_hospitality
			doctrine = tenet_harpy_hunt
			doctrine = tenet_adaptive
		}		
	}
}