﻿
#reusing the claim throne custom loc to be a generic effect
GetMythicalFounder = { # Anbennar TODO: add our founders
	
	type = character
	
	random_valid = yes
	
	text = { # Castan the Progenitor
		trigger = {
			OR = {
				this = title:e_castanor
				this = title:k_cast
				this = title:k_anor
				this = title:k_castonath
				this = title:k_castellyr
			}
		}
		localization_key = MythicalFounder_castan_the_progenitor
	}
	
	text = { # Godrac the Invader
		trigger = {
			this = title:k_gawed
		}
		localization_key = MythicalFounder_godrac_the_invader
	}
	
	text = { # Lorenan the Great
		trigger = {
			OR = {
				this = title:e_lencenor
				this = title:k_lorent
			}
		}
		localization_key = MythicalFounder_lorenan_the_great
	}
	
	text = { # Armoc the Wyvernrider
		trigger = {
			this = title:k_verne
		}
		localization_key = MythicalFounder_armoc_the_wyvernrider
	}
	
	text = { # Elikhet
		trigger = {
			OR = { # Elikhet
				this = title:e_kheterata
				this = title:k_kheterata
			}
		}
		localization_key = MythicalFounder_elikhet
	}
	
	# ADD MORE !!!!!!!
	
	text = {
		localization_key = MythicalFounder_fallback
	}
}
