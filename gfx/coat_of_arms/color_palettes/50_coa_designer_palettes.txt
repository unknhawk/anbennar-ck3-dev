﻿# Colors used in the CoA designer.
# The order defines the order in which they are listed in the palette.
#
# Format:
# coa_designer_background_colors = {
#     color_name = {} # must refer to a named color
#     color_name = {}
#     # ...
# }

# Background colors available in palette in CoA designer.
# Do not remove this entry.
coa_designer_background_colors = {
	red = {}
	orange = {}
	yellow = {}
	yellow_light = {}
	white = {}
	grey = {}
	black = {}
	brown = {}
	green = {}
	green_light = {}
	blue_light = {}
	blue = {}
	purple = {}
	
	# Anbennar
	damerian_blue = {}
	damerian_blue_light = {}
	damerian_black = {}
	damerian_white = {}
	pearlsedge_blue = {}
	pearlsedge_pearl = {}
	arannese_blue = {}
	gawedi_blue = {}
	busilari_orange = {}
	blademarch_gray = {}
	blademarch_yellow = {}
	eborthili_gold = {}
	verne_red = {}
	verne_wyvern_red = {}
	verne_beige = {}
	lorentish_red = {}
	lorentish_red_dark = {}
	lorentish_green = {}
	lorentish_green_dark = {}
	lorentish_lilac = {}
	lorentish_gold = {}
	enteben_red = {}
	derannic_purple = {}
	derannic_pink = {}
	roilsardi_red = {}
	roilsardi_green = {}
	corvurian_red_dark = {}
	corvurian_red_light = {}
	iochand_yellow = {}
	iochand_blue = {}
	iochand_green = {}
	wexonard_purple = {}
	ibevar_blue = {}
	dark_hunter_green = {}
	castanorian_dragon_silver = {}
	
	beige = {}
	purpure = {}
	pink = {}
	brown_dark = {}
}
