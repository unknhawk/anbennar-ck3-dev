﻿

namespace = anb_coming_of_age

#Elven Adult - by JayBean
anb_coming_of_age.0001 = {
	title = anb_coming_of_age.0001.t
	desc = anb_coming_of_age.0001.desc
	theme = education
	left_portrait = {
		character = root
		triggered_animation = {
			trigger = {
				has_focus = education_martial
			}
  			animation = marshal
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_learning
			}
  			animation = personality_rational
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_stewardship
			}
  			animation = steward
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_intrigue
			}
  			animation = spymaster
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_diplomacy
			}
  			animation = flirtation_left
  		}
		animation = idle
	}
	override_background = {
		trigger = {
			has_focus = education_martial
		}
		reference = courtyard
	}
	override_background = {
		trigger = {
			has_focus = education_diplomacy
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = council_chamber
	}
	override_background = {
		trigger = {
			has_focus = education_diplomacy
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = throne_room
	}
	override_background = {
		trigger = {
			has_focus = education_intrigue
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = physicians_study
	}
	override_background = {
		trigger = {
			has_focus = education_intrigue
			culture = {
				NOR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = relaxing_room
	}
	override_background = {
		trigger = {
			has_focus = education_stewardship
		}
		reference = market
	}
	override_background = {
		trigger = {
			has_focus = education_learning
			culture = {
				NOR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = temple
	}
	
	trigger = {
		has_trait = race_elf
	}

	immediate = {
		play_music_cue = "mx_cue_positive_effect"
	}

	option = {
		name = anb_coming_of_age.0001.a

		add_trait_xp = {
			trait = race_elf
			value = 30	#if they are at 20, it gets them to 50 which is the Elf Adult
		}
	}
}


#Elven Elder - by JayBean
anb_coming_of_age.0002 = {
	title = anb_coming_of_age.0002.t
	desc = anb_coming_of_age.0002.desc
	theme = education
	left_portrait = {
		character = root
		triggered_animation = {
			trigger = {
				has_focus = education_martial
			}
  			animation = marshal
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_learning
			}
  			animation = personality_rational
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_stewardship
			}
  			animation = steward
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_intrigue
			}
  			animation = spymaster
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_diplomacy
			}
  			animation = flirtation_left
  		}
		animation = idle
	}
	override_background = {
		trigger = {
			has_focus = education_martial
		}
		reference = courtyard
	}
	override_background = {
		trigger = {
			has_focus = education_diplomacy
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = council_chamber
	}
	override_background = {
		trigger = {
			has_focus = education_diplomacy
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = throne_room
	}
	override_background = {
		trigger = {
			has_focus = education_intrigue
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = physicians_study
	}
	override_background = {
		trigger = {
			has_focus = education_intrigue
			culture = {
				NOR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = relaxing_room
	}
	override_background = {
		trigger = {
			has_focus = education_stewardship
		}
		reference = market
	}
	override_background = {
		trigger = {
			has_focus = education_learning
			culture = {
				NOR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = temple
	}
	
	trigger = {
		has_trait = race_elf
	}

	immediate = {
		play_music_cue = "mx_cue_positive_effect"
	}

	option = {
		name = anb_coming_of_age.0002.a

		add_trait_xp = {
			trait = race_elf
			value = 100	#gets them to 100 which is Elder
		}

		#TODO Personality change if Ambitious, become Content. If Diligent, become Lazy, if Wrathful become Calm etc

		ai_chance = {
			base = 75
		}
	}

	option = {
		name = anb_coming_of_age.0002.b	#just retire early

		add_trait_xp = {
			trait = race_elf
			value = 100	#gets them to 100 which is Elder
		}
		
		death = {
			death_reason = death_retirement
		}

		ai_chance = {
			base = 25
			
			modifier = {	#just a simple reason that elves can take this route
				factor = 3
				trigger = {
					OR = {
						has_trait = content
						has_trait = lazy
						has_trait = impatient
						has_trait = reclusive
					}
				}
			}
			modifier = {	#just a simple reason that elves can take this route
				factor = 0
				trigger = {
					OR = {
						has_trait = ambitious
						ai_energy > 100
					}
				}
			}
		}
	}
}


#Elven Child - by JayBean
anb_coming_of_age.0003 = {
	title = anb_coming_of_age.0003.t
	desc = anb_coming_of_age.0003.desc
	theme = education
	left_portrait = {
		character = root
		triggered_animation = {
			trigger = {
				has_focus = education_martial
			}
  			animation = marshal
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_learning
			}
  			animation = personality_rational
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_stewardship
			}
  			animation = steward
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_intrigue
			}
  			animation = spymaster
  		}
		triggered_animation = {
			trigger = {
				has_focus = education_diplomacy
			}
  			animation = flirtation_left
  		}
		animation = idle
	}
	override_background = {
		trigger = {
			has_focus = education_martial
		}
		reference = courtyard
	}
	override_background = {
		trigger = {
			has_focus = education_diplomacy
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = council_chamber
	}
	override_background = {
		trigger = {
			has_focus = education_diplomacy
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = throne_room
	}
	override_background = {
		trigger = {
			has_focus = education_intrigue
			culture = {
				OR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = physicians_study
	}
	override_background = {
		trigger = {
			has_focus = education_intrigue
			culture = {
				NOR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = relaxing_room
	}
	override_background = {
		trigger = {
			has_focus = education_stewardship
		}
		reference = market
	}
	override_background = {
		trigger = {
			has_focus = education_learning
			culture = {
				NOR = {
					has_building_gfx = western_building_gfx
					has_building_gfx = mediterranean_building_gfx
				}
			}
		}
		reference = temple
	}
	
	trigger = {
		has_trait = race_elf
	}

	immediate = {
		play_music_cue = "mx_cue_positive_effect"
	}

	option = {
		name = anb_coming_of_age.0003.a

		add_trait_xp = {
			trait = race_elf
			value = 20	#gets them to 20 which is Child
		}

		ai_chance = {
			base = 75
		}
	}

	# option = {
	# 	name = anb_coming_of_age.0002.b	#skip if you're cool enough

	# 	trigger = {
	# 		has_trait = ambitious
	# 	}

	# 	add_trait_xp = {
	# 		trait = race_elf
	# 		value = 50	#gets them to 50 which is Adult
	# 	}
		

	# 	ai_chance = {
	# 		base = 50
	# 	}
	# }
}
