﻿# Kingdom of Deranne dynasties

###############
# Dynasty Derhilding

derhilde_reaverqueen = { # Derhilde "Reaverqueen"
	name = "Derhilde"
	dynasty = dynasty_derhilding
	religion = skaldhyrric_faith
	culture = olavish
	female = yes
	
	trait = race_human
	trait = education_martial_3
	trait = ambitious
	trait = deceitful
	trait = brave
	trait = reaver
	trait = shieldmaiden
	
	825.12.22 = {
		birth = yes
	}
	
	869.2.26 = {
		give_nickname = nick_reaverqueen
	}
	
	892.11.12 = {
		death = "892.11.12"
	}
}

deranne_0001 = { # Askel Derhilding, King of Deranne
	name = "Askel"
	dynasty = dynasty_derhilding
	religion = court_of_adean
	culture = olavish
	
	trait = race_human
	trait = education_stewardship_2
	trait = temperate
	trait = content
	trait = trusting
	
	mother = derhilde_reaverqueen
	
	853.3.16 = {
		birth = yes
	}
	
	912.11.16 = {
		death = "912.11.16"
	}
}

deranne_0002 = { # Katarina Derhilding
	name = "Katarina"
	dynasty = dynasty_derhilding
	religion = skaldhyrric_faith
	culture = olavish
	female = yes
	
	trait = race_human
	
	mother = derhilde_reaverqueen
	
	857.7.14 = {
		birth = yes
	}
	
	900.1.27 = {
		death = "900.1.27"
	}
}

deranne_0003 = { # Gunnar Derhilding, King of Deranne
	name = "Gunnar"
	dynasty = dynasty_derhilding
	religion = skaldhyrric_faith
	culture = olavish
	
	trait = race_human
	trait = education_martial_3
	trait = brave
	trait = arrogant
	trait = wrathful
	trait = reaver
	
	mother = derhilde_reaverqueen
	
	865.3.30 = {
		birth = yes
	}
	
	888.3.9 = {
		add_spouse = pearlman_0004
	}
	
	924.5.21 = {
		death = "924.5.21"
	}
}

deranne_0004 = { # Hjalmar Derhilding, ex King of Deranne, Duke of Darom
	name = "Hjalmar"
	dynasty = dynasty_derhilding
	religion = skaldhyrric_faith
	culture = olavish
	
	trait = race_human
	trait = education_intrigue_1
	trait = deceitful
	trait = vengeful
	trait = craven
	
	father = deranne_0003
	mother = pearlman_0004
	
	892.11.23 = {
		birth = yes
	}
	
	930.5.26 = {
		death = {
			death_reason = death_execution
			killer = deranne_0009
		}
	}
}

deranne_0005 = { # Haakon Derhilding, Duke of Darom
	name = "Haakon"
	dynasty = dynasty_derhilding
	religion = skaldhyrric_faith
	culture = derannic
	
	trait = race_human
	trait = education_stewardship_1
	trait = content
	trait = patient
	trait = compassionate
	
	father = deranne_0003
	mother = pearlman_0004
	
	899.1.24 = {
		birth = yes
	}
	
	956.8.19 = {
		death = "956.8.19"
	}
}

deranne_0006 = { # Daromdan Derhilding
	name = "Daromdan"
	dynasty = dynasty_derhilding
	religion = skaldhyrric_faith
	culture = derannic
	
	trait = race_human
	trait = fickle
	trait = impatient
	trait = generous
	
	father = deranne_0005
	
	940.1.24 = {
		birth = yes
	}
	
	955.7.31 = {
		death = "955.7.31"
	}
}

deranne_0007 = { # Maurice Derhilding
	name = "Maurice"
	dynasty = dynasty_derhilding
	religion = court_of_adean
	culture = derannic
	
	trait = race_human
	trait = education_martial_1
	trait = brave
	trait = impatient
	trait = wrathful
	
	father = deranne_0001
	
	882.3.19 = {
		birth = yes
	}
	
	912.11.16 = {
		death = {
			death_reason = death_duel
			killer = deranne_0003
		}
	}
}

deranne_0008 = { # Anna Derhilding
	name = "Anna"
	dynasty = dynasty_derhilding
	religion = whiterose_court
	culture = derannic
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = gregarious
	trait = honest
	trait = temperate
	
	father = deranne_0001
	
	890.7.25 = {
		birth = yes
	}
	
	907.9.11 = {
		add_spouse = lorentis_0003
	}
	
	951.2.17 = {
		death = "951.2.17"
	}
}

# House of Deranne, lencorized house

deranne_0009 = { # Tomar I of Deranne, King of Deranne
	name = "Tomar"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic
	
	trait = race_human
	trait = education_stewardship_2
	trait = diligent
	trait = just
	trait = compassionate
	
	father = deranne_0001
	
	900.4.10 = {
		birth = yes
	}
	
	957.8.4 = {
		death = "957.8.4"
	}
}

deranne_0010 = { # Artus of Deranne, King of Deranne
	name = "Artus"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic
	
	trait = race_human
	
	father = deranne_0009
	
	920.2.16 = {
		birth = yes
	}
	
	963.6.26 = {
		death = "963.6.26"
	}
}

deranne_0011 = { # Tomar II of Deranne, King of Deranne
	name = "Tomar"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic
	
	trait = race_human
	
	father = deranne_0010
	
	942.4.21 = {
		birth = yes
	}
	
	990.9.20 = {
		death = "990.9.20"
	}
}

deranne_0012 = { # Kalbard of Deranne, current King of Deranne
	name = "Kalbard"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic

	diplomacy = 4
	martial = 13
	stewardship = 7
	intrigue = 4
	learning = 14
	
	trait = race_human
	trait = education_learning_2
	trait = temperate
	trait = stubborn
	trait = chaste
	
	father = deranne_0011
	
	960.10.3 = {
		birth = yes
	}

	983.2.10 = {
		add_spouse = caylenoris_0009
	}
}

deranne_0016 = { # Cora, child of King Kalbard, queen of Reveria
	name = "Cora"
	dynasty_house = house_deranne
	religion = reverian_cult
	culture = derannic
	female = yes

	trait = race_human
	trait = education_stewardship_2
	trait = patient
	trait = lazy
	trait = compassionate
	trait = stuttering
	
	father = deranne_0012
	mother = caylenoris_0009
	
	991.1.3 = {
		birth = yes
	}
}

deranne_0017 = { # Prince Erland, Heir of King Kalbard
	name = "Erland"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = education_stewardship_2
	trait = temperate
	trait = wrathful
	trait = ambitious
	
	father = deranne_0012
	mother = caylenoris_0009
	
	990.1.6 = {
		birth = yes
	}
	
	1008.1.1 = {
		add_spouse = pearlsguard_0003
	}
}

deranne_0018 = { # Prince Otto
	name = "Otto"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = education_martial_3
	trait = stubborn
	trait = brave
	trait = paranoid
	
	father = deranne_0012
	mother = caylenoris_0009
	
	990.2.11 = {
		birth = yes
	}
}

deranne_0019 = { # Princess Elisa
	name = "Otto"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic
	female = yes

	trait = race_human
	trait = education_learning_2
	trait = fickle
	trait = lustful
	trait = forgiving
	
	father = deranne_0012
	mother = caylenoris_0009
	
	998.2.28 = {
		birth = yes
	}
}

deranne_0020 = { # Princess Marien, daughter of Erland
	name = "Marien"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic
	female = yes

	trait = race_human

	trait = physique_good_1
	trait = charming
	
	father = deranne_0017
	mother = pearlsguard_0003
	
	1008.8.6 = {
		birth = yes
	}
}

deranne_0021 = { # Prince Tomar, son of Erland
	name = "Tomar"
	dynasty_house = house_deranne
	religion = whiterose_court
	culture = derannic

	trait = race_human

	trait = bossy
	
	father = deranne_0017
	mother = pearlsguard_0003
	
	1014.10.14 = {
		birth = yes
	}
}

###############
# Dynasty Steedspear

steedspear_0001 = { # Count Edmond of Steedspear
	name = "Edmond"
	dynasty = dynasty_steedspear
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = diligent
	trait = brave
	trait = just
	trait = education_martial_4
	
	974.8.14 = {
		birth = yes
	}

	1010.2.3 = {
		death = "1010.2.3"
	}
}

deranne_0013 = { # Margeret of Steedspear
	name = "Margeret"
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = calm
	trait = honest
	trait = just
	trait = education_stewardship_2

	female =  yes

	975.3.12 = {
		birth = yes
	}

	996.3.10 = {
		add_spouse = steedspear_0001
	}
}

steedspear_0002 = { # Count Artus of Steedspear
	name = "Artus"
	dynasty = dynasty_steedspear
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = brave
	trait = honest
	trait = just
	trait = education_martial_3

	father = steedspear_0001

	mother = deranne_0013

	998.4.25 = {
		birth = yes
	}
}

mocard_0006 = { # Countess Marie of Steedspear
	name = "MariE_"
	dynasty = dynasty_mocard
	religion = whiterose_court
	culture = derannic
	female = yes

	trait = race_human
	trait = calm
	trait = gregarious
	trait = humble
	trait = education_diplomacy_1

	father = mocard_0003

	1003.2.18 = {
		birth = yes
	}

	1021.1.1 = {
		add_spouse = steedspear_0002
	}
}

###############
# Dynasty Fjorhavn

fjorhavn_0001 = { # Lucian Fjorhavn
	name = "Lucian"
	dynasty = dynasty_fjorhavn
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = gregarious
	trait = humble
	trait = patient
	trait = education_diplomacy_4

	953.9.13 = {
		birth = yes
	}

	995.5.8 = {
		death = "995.5.8"
	}
}

fjorhavn_0002 = { # Count Fredéric of Fjorhavn
	name = "FredE_ric"
	dynasty = dynasty_fjorhavn
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = honest
	trait = craven
	trait = diligent
	trait = education_stewardship_3

	father = fjorhavn_0001

	975.8.1 = {
		birth = yes
	}

	1011.1.6 = {
		death = "1011.1.6"
	}
}

deranne_0015 = { # Claire of Fjorhavn
	name = "Claire"
	religion = whiterose_court
	culture = derannic
	female = yes

	trait = race_human
	trait = brave
	trait = honest
	trait = just
	trait = education_learning_1

	972.5.1 = {
		birth = yes
	}

	1013.11.16 = {
		death = "1013.11.16"
	}
	
	1001.2.13 = {
		add_spouse = fjorhavn_0002
	}
}

fjorhavn_0003 = { # Tomar Fjorhavn
	name = "Tomar"
	dynasty = dynasty_fjorhavn
	religion = whiterose_court
	culture = derannic

	trait = race_human
	trait = ambitious
	trait = greedy
	trait = deceitful
	trait = education_intrigue_3

	father = fjorhavn_0002

	1003.5.1 = {
		birth = yes
	}
}

fjorhavn_0004 = { # Robin Fjorhavn
	name = "Robin"
	dynasty = dynasty_fjorhavn
	religion = whiterose_court
	culture = derannic

	trait = race_human

	father = fjorhavn_0002

	1005.6.12 = {
		birth = yes
	}
}

###############
# Dynasty Mocard

mocard_0001 = { # Oskar
	name = "Oskar"
	dynasty = dynasty_mocard
	religion = whiterose_court
	culture = derannic
	
	trait = race_human
	trait = education_stewardship_3
	trait = wrathful
	trait = ambitious
	trait = diligent
	
	921.03.07 = {
		birth = yes
	}
	
	989.10.02 = {
		death = "989.10.02"
	}
}


mocard_0002 = { # Tomás
	name = "TomA_s"
	dynasty = dynasty_mocard
	religion = whiterose_court
	culture = derannic
	
	trait = race_human
	trait = education_martial_2
	trait = gluttonous
	trait = zealous
	trait = brave
	father = mocard_0001
	
	951.07.04 = {
		birth = yes
	}
	
	1001.11.03 = {
		death = "1001.11.03"
	}
}

mocard_0003 = { # Bertran, count of Darom
	name = "Bertran"
	dynasty = dynasty_mocard
	religion = whiterose_court
	culture = derannic
	dna = bertran_mocard
	
	trait = race_human
	trait = education_stewardship_2
	trait = diligent
	trait = craven
	trait = humble
	 
	father = mocard_0002
	
	978.09.01 = {
		birth = yes
	}
}

mocard_0004 = { # Robert, brother of Bertran
	name = "Bertran"
	dynasty = dynasty_mocard
	religion = whiterose_court
	culture = derannic
	
	trait = race_human
	trait = education_diplomacy_2
	trait = honest
	trait = gregarious
	trait = content
	
	father = mocard_0002
	
	984.09.26 = {
		birth = yes
	}
}


mocard_0005 = { # Artur, son of Bertran
	name = "Artur"
	dynasty = dynasty_mocard
	religion = whiterose_court
	culture = derannic
	sexuality = homosexual
	
	trait = race_human
	trait = education_intrigue_2
	trait = deceitful
	trait = shy
	trait = just
	
	father = mocard_0003
	
	1001.09.09 = {
		birth = yes
	}
}

fjorhavn_0006 = { # Olga, wife of Bertran
	name = "Olga"
	dynasty = dynasty_fjorhavn
	religion = whiterose_court
	culture = derannic
	female = yes

	trait = race_human
	trait = education_stewardship_2
	
	father = fjorhavn_0001

	983.6.3 = {
		birth = yes
	}
	998.7.9 = {
		add_spouse = mocard_0003
	}
}