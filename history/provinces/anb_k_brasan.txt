#k_brasan
##d_sap_brasan
###c_brasan
565 = {		#Brasan - Turn into City

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = castle_holding

    # History
}

6477 = {		#Brasankar

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = city_holding

    # History
}

6478 = {		#Eduz-Betdaris

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = church_holding

    # History
}

6479 = {		#Naba-Abadazar

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = none

    # History
}

6480 = {		#Aqat-Abadazar

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = city_holding

    # History
}

###c_djinnkalis
564 = {		#Djinnkalis

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = castle_holding

    # History
}

6481 = {		#Kidalam

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = none

    # History
}

6482 = {		#Izatisa

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = city_holding

    # History
}

###c_rakkubtu
563 = {		#Rakkubtu

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = city_holding

    # History
}

6497 = {		#Abittu

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = none

    # History
}

6498 = {		#Eduz-Mahara

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = church_holding

    # History
}

6499 = {		#Rakkukar

    # Misc
    culture = brasanni
    religion = jaherian_cults
	holding = city_holding

    # History
}


##d_baru_brasan
###c_brasan_tanuz
570 = {		#Brasan Tanuz

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = castle_holding

    # History
}

6503 = {		#Sahtawez

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = city_holding

    # History
}

6504 = {		#Miwsah

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = none

    # History
}

###c_kadumar
568 = {		#Kadumar

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = city_holding

    # History
}

6505 = {		#Awatklum

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = none

    # History
}

6506 = {		#Hasawy

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = city_holding

    # History
}

##d_len_brasan
###c_suran_narit
572 = {		#Suran Narit

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = castle_holding

    # History
}

6507 = {		#Lenaritbar

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = city_holding

    # History
}

6508 = {		#Harnarit

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = none

    # History
}

###c_urasum
567 = {		#Urasum

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = castle_holding

    # History
}

6500 = {		#Qurklumar

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = city_holding

    # History
}

6501 = {		#Namarisit

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = none

    # History
}

6502 = {		#Naba-Urasum

    # Misc
    culture = brasanni
    religion = rite_of_the_eternal_home
	holding = church_holding

    # History
}

###c_aqatabtu
562 = {		#Aqatabtu

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6515 = {		#Lukan-nisubtu

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

6516 = {		#Abbaalbar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6517 = {		#Tabaskalum

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}