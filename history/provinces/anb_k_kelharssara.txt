#k_kelharssara
##d_gelkalis
###c_gelkalis
665 = {		#b_gelkalis

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2946 = {    #b_rusqikkis

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = church_holding

    # History
}

2947 = {    #b_sarku

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2948 = {    #b_elisiyya

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2949 = {    #b_eduz_leuziha

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_as_mis
664 = {		#b_as_mis

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2950 = {    #b_tepakkaru

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2951 = {    #b_warzzila

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2952 = {    #b_isana

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

##d_lawassar
###c_lawassar
2956 = {    #b_lawassar

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

655 = {		#b_lazzaward

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2954 = {    #b_kuresim_kisbar

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2955 = {    #b_kahrulam

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_peruskam
656 = {		#b_peruskam

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2957 = {    #b_unnuziya

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2958 = {    #b_hawazubtu

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_kihayanbar
653 = {		#b_kihayanbar

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2959 = {    #b_eduz_alariya

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2960 = {    #b_hayanalla

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

2961 = {    #b_lakka

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

##d_elusadul
###c_elusadul
654 = {		#b_elusadul

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2962 = {    #b_hattizza

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2963 = {    #b_luqtibar

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2964 = {    #b_attalayya

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = city_holding

    # History
}

###c_akurmbag
652 = {		#b_akurmbag

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = castle_holding

    # History
}

2965 = {    #b_eduz_zasselu

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2966 = {    #b_innuhassa

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

##d_arkasul
###c_arkasul
2967 = {    #b_arkasul

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

671 = {		#b_eduz_zersubtu

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = tribal_holding

    # History
}

2968 = {    #b_nesazan

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2969 = {    #b_rakasallu

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_hranapas
668 = {		#b_hranapas

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = tribal_holding

    # History
}

2970 = {    #b_sadurran

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2971 = {    #b_hasaklen

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2972 = {    #b_leqabu

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

###c_elisan
669 = {		#b_elisan

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = tribal_holding

    # History
}

2973 = {    #b_aslusubtu

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}

2974 = {    #b_sadrahlu

    # Misc
    culture = gelkar
    religion = cult_of_the_wise_master
	holding = none

    # History
}