#k_westmoors
##d_westmoor
###c_brontay_point
191 = {		#Brontay Point

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1433 = {

    # Misc
    holding = city_holding

    # History

}
1435 = {

    # Misc
    holding = none

    # History

}

###c_topmanor_grange
1434 = {

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History

}
1440 = {

    # Misc
    holding = none

    # History

}
1489 = {

    # Misc
    holding = church_holding

    # History

}

###c_cottersea
192 = {		#Cottersea

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1438 = {

    # Misc
    holding = none

    # History

}
1439 = {

    # Misc
    holding = none

    # History

}

###c_leapdon
193 = {		#Leapdon

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1490 = {

    # Misc
    holding = none

    # History

}
1491 = {

    # Misc
    holding = none

    # History

}
1492 = {

    # Misc
    holding = none

    # History

}
1445 = {

    # Misc
    holding = church_holding

    # History

}

###c_wispmire
194 = {		#Wispmire

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1448 = {

    # Misc
    holding = city_holding

    # History

}
1449 = {

    # Misc
    holding = none

    # History

}
1450 = {

    # Misc
    holding = none

    # History

}

###c_bleakpass
345 = {		#Bleakpass

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1451 = {

    # Misc
    holding = church_holding

    # History

}
1452 = {

    # Misc
    holding = none

    # History

}
1453 = {

    # Misc
    holding = none

    # History

}

##d_moorhills
###c_foulers_fen
195 = {		#Fouler's Fen

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1446 = {

    # Misc
    holding = none

    # History

}
1447 = {

    # Misc
    holding = city_holding

    # History

}

###c_redferne_hill
196 = {		#Redferne Hill

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1456 = {

    # Misc
    holding = none

    # History

}

###c_woldenmarck
1454 = {

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History

}
1455 = {

    # Misc
    holding = none

    # History

}

##d_beronmoor
###c_beron_heights
201 = {		#Beron Heights

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1460 = {

    # Misc
    holding = city_holding

    # History

}
1461 = {

    # Misc
    holding = none

    # History

}
1462 = {

    # Misc
    holding = none

    # History

}

###c_houndsmoor
1457 = {

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History

}
1458 = {

    # Misc
    holding = city_holding

    # History

}
1459 = {

    # Misc
    holding = none

    # History

}

###c_cockerwall
203 = {		#Cockerwall

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1469 = {

    # Misc
    holding = none

    # History

}
1484 = {

    # Misc
    holding = none

    # History

}

###c_bryon_hall
1470 = {

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History

}
1481 = {

    # Misc
    holding = city_holding

    # History

}
1483 = {

    # Misc
    holding = none

    # History

}

###c_moorton
202 = {		#Moorton

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1463 = {

    # Misc
    holding = none

    # History

}
1464 = {

    # Misc
    holding = none

    # History

}
1465 = {

    # Misc
    holding = none

    # History

}
1466 = {

    # Misc
    holding = city_holding

    # History

}

###c_heathcliff
1485 = {	#Hareton

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History

}
1486 = {

    # Misc
    holding = none

    # History

}

###c_edgemoor
204 = {		#Edgemoor

    # Misc
    culture = moorman
    religion = moors_cult
	holding = castle_holding

    # History
}
1487 = {

    # Misc
    holding = church_holding

    # History

}
1488 = {

    # Misc
    holding = none

    # History

}
