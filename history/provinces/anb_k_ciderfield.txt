#k_ciderfield
##d_pearview
###c_pearview
147 = {		

    # Misc
	holding = none

    # History

}
1233 = {

    # Misc
    holding = city_holding

    # History

}
1234 = {    #Pearview

    # Misc
    culture = ciderfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History

}
1235 = {

    # Misc
    holding = none

    # History

}
1236 = {

    # Misc
    holding = none

    # History

}
1237 = {

    # Misc
    holding = church_holding

    # History

}


###c_butterburn
103 = {		#Butterburn

    # Misc
    culture = ciderfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History

}
1238 = {

    # Misc
    holding = none

    # History

}
1239 = {

    # Misc
    holding = church_holding

    # History

}
1240 = {

    # Misc
    holding = none

    # History

}

141 = {		#Turnwell

    # Misc
	holding = city_holding

    # History
}

###c_newcobble
165 = {		#Newcobble

    # Misc
    culture = ciderfoot_halfling
    religion = small_temple
	holding = city_holding

    # History

}

2698 = {
    # Misc

	holding = church_holding

    # History
}

##d_appleton
###c_appleton
139 = {		#Appleton

    # Misc
    culture = ciderfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History

}
1241 = {

    # Misc
    holding = city_holding

    # History

}
1242 = {

    # Misc
    holding = church_holding

    # History

}
1243 = {

    # Misc
    holding = none

    # History

}

###c_cowskeep
137 = {		#Cowskeep

    # Misc
    culture = ciderfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History

}
1246 = {

    # Misc
    holding = none

    # History

}
1247 = {

    # Misc
    holding = church_holding

    # History

}
1248 = {

    # Misc
    holding = city_holding

    # History

}

###c_littlebrook
163 = {		#Littlebrook

    # Misc
    culture = ciderfoot_halfling
    religion = small_temple
	holding = castle_holding

    # History

}
1244 = {

    # Misc
    holding = city_holding

    # History

}
