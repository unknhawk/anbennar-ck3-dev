k_busilar = {
	1000.1.1 = {
		change_development_level = 8
	}
	1014.1.1 = {
		liege = e_bulwar
		holder = sun_elvish0040 # Thranduir Tirenzuir
	}
}

c_busilar = {
	1000.1.1 = {
		change_development_level = 10
		holder = taulez_0001
	}
}

c_carverhouse = {
	1000.1.1 = {
		holder = taulez_0001
	}
}

c_lioncost = {
	1000.1.1 = {
		change_development_level = 9
	}
}

c_derancestir = {
	800.1.1 = {
		liege = "k_eborthil"
	}
	1000.1.1 = {
		change_development_level = 9
	}
	1019.1.1 = {
		holder = 599	#Deran of Derancestir; placeholder date for the acquiral of the region
	}
	
}

c_lions_pass = {
	1000.1.1 = {
		change_development_level = 9
	}
	1014.1.1 = {
		liege = k_eborthil
		# holder = 16 # Narawen "the Traveller" Silnara
		holder = 17 # Eborian # placeholder
	}
}

c_ohitkes = {
	1016.1.1 = {
		holder = mircatares_0001
		liege = "k_eborthil"
	}
}

c_port_jaher = {
	1000.1.1 = {
		change_development_level = 13
	}
	1014.1.1 = {
		liege = e_bulwar
		holder = sun_elvish0040 # Thranduir Tirenzuir
	}
}

d_hapaine = {
	1000.1.1 = {
		change_development_level = 10
	}
}

c_hapaine = {
	1000.1.1 = {
		change_development_level = 11
	}
}

d_lorin = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_lorincrag = {
	1000.1.1 = {
		liege = k_eborthil
	}
	1016.1.1 = {
		holder = seaborn_0001
	}
}

c_lorinhap = {
	1000.1.1 = {
		holder = taulez_0001
	}
}

d_canno = {
	1000.1.1 = {
		change_development_level = 7
		holder = taulez_0001
	}
}

c_cannohap = {
	1000.1.1 = {
		holder = taulez_0001
	}
}

c_taulos = {
	1000.1.1 = {
		holder = taulez_0001
	}
}

c_khenahap = {
	1000.1.1 = {
		change_development_level = 6
		holder = hortoselez_0001
	}
}

d_hortosare = {
	1000.1.1 = {
		change_development_level = 7
		holder = hortoselez_0001
	}
}

c_hortosare = {
	1000.1.1 = {
		change_development_level = 6
		holder = hortoselez_0001
	}
}

c_stonehold = {
	1000.1.1 = {
		change_development_level = 6
		holder = hortoselez_0001
	}
}

c_enkets_pass = {
	1000.1.1 = {
		holder = hortoselez_0001
	}
}

c_elsine = {
	1000.1.1 = {
		holder = hortoselez_0001
	}
}

c_oristes = {
	1016.1.1 = {
		holder = rivares_0003
		liege = "k_eborthil"
	}
}