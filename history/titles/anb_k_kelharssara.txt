d_gelkalis = {
	1003.4.9 = {
		holder = bulwari0012 # Naram szel-Gelkalis
	}
	1018.3.6 = { 
		liege = e_harpylen 
	}
}

c_elusadul = {
	1000.1.1 = { change_development_level = 15 }
}

c_akurmbag = {
	1000.1.1 = { change_development_level = 13 }
}

c_kihayanbar = {
	1000.1.1 = { change_development_level = 13 }
}

c_lawassar = {
	1000.1.1 = { change_development_level = 17 }
}

c_peruskam = {
	1000.1.1 = { change_development_level = 14 }
}

c_gelkalis = {
	1000.1.1 = { change_development_level = 17 }
	1003.4.9 = {
		holder = bulwari0012 # Naram szel-Gelkalis
	}
}

c_as_mis = {
	1000.1.1 = { change_development_level = 13 }
}

c_hranapas = {
	1000.1.1 = { change_development_level = 14 }
}

c_arkasul = {
	1000.1.1 = { change_development_level = 16 }
}

c_elisan = {
	1000.1.1 = { change_development_level = 10 }
}