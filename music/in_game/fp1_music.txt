﻿# Anbennar: commenting vanilla stuff (hopefully doesn't cause problems to those without dlc)

#### TNL MUSC

mx_raid = {
	music = "event:/DLC/FP1/MUSIC/cuetracks/mx_raid"
	pause_factor = 35
}

mx_drakkar = {
	music = "event:/DLC/FP1/MUSIC/moodtrack/mx_drakkar"
	mood = yes
	pause_factor = 15
	can_be_interrupted = yes
	is_prioritized_mood = yes

	is_valid = {
		# OR = {
			# culture = { has_cultural_pillar = heritage_north_germanic }
			# religion = religion:germanic_religion
		# }
		# Anbennar
		culture = { has_cultural_pillar = heritage_gerudian }
	}
}

mx_scandinavia = {
	music = "event:/DLC/FP1/MUSIC/moodtrack/mx_scandinavia"
	mood = yes
	pause_factor = 15
	can_be_interrupted = yes
	is_prioritized_mood = yes

	is_valid = {
		# OR = {
			# culture = { has_cultural_pillar = heritage_north_germanic }
			# religion = religion:germanic_religion
		# }
		# Anbennar
		culture = { has_cultural_pillar = heritage_gerudian }
	}
}

mx_thefeast = {
	music = "event:/DLC/FP1/MUSIC/moodtrack/mx_thefeast"
	mood = yes
	pause_factor = 15
	can_be_interrupted = yes
	is_prioritized_mood = yes

	is_valid = {
		# OR = {
			# culture = { has_cultural_pillar = heritage_north_germanic }
			# religion = religion:germanic_religion
		# }
		# Anbennar
		culture = { has_cultural_pillar = heritage_gerudian }
	}
}
